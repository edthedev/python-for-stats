# which python3
PYTHON3=/usr/local/bin/python3
PYTHON3_PATH=/usr/local/bin/python3
PIP3=./ve/bin/pip3
IPYTHON=ve/bin/ipython3

ve:
	virtualenv -p $(PYTHON3) ve

requirements:
	$(PIP3) install --exists-action i -r requirements.txt

install_skikit:
	$(PIP3) install --allow-external scikit-learn scikit-learn

setup: ve requirements

notebook:
	$(IPYTHON) notebook
